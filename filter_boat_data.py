#!/usr/bin/python3
"""
    Boat Data Cleaning Procedures
"""
import sys
import os
import numpy as np
import mxnet as mx
import cv2
import time
import threading
import multiprocessing
from multiprocessing import Pool
import concurrent.futures
from mxnet.gluon.data.vision import transforms
import gluoncv
from gluoncv.data.transforms.presets.segmentation import test_transform
from gluoncv.utils.viz import get_color_pallete
from gluoncv import model_zoo, data, utils
from gluoncv.data.transforms.mask import fill
from tqdm import tqdm
import logging
import datetime
from running_stats import RunningStat
from mask_rcnn import MaskRCNN
from deeplab_processor import DeepLabProcessor
import gc
import argparse
import queue

#---------#
# GLOBALS #
#---------#
# Filtering Thresholds - for a better explanation of these thresholds, seek the documentation of this algorithm on Dropbox
LAND_THRESH = 6.0                   # Maximum percentage of image pixels for land segments
BUILDING_THRESH = 4.0               # Maximum percentage of image pixels for building segments
LB_RATIO_THRESH = 12.0              # Maximum percentage of image pixels for both land and building segments
LB_NON_BOAT_RATIO_THRESH = 30.0     # Maximum percentage of image pixels for land and building segments for non-boat segments
WATER_NON_BOAT_RATIO_THRESH = 70.0  # Minimum percentage of image pixels for water for non-boat segments
PERSON_RATIO_THRESH = 2.5           # Maximum percentage of image pixels for people
BOAT_THRESH = 10                    # Minimum percentage of image pixels for boats
BOAT_PIXEL_CNT_THRESH = 10000       # Minimum number of image pixels for boats
BOAT_UPPER_BOUND_THRESH = 55        # Maximum percentage of image pixels attributed to a boat
BOAT_LB_THRESHOLD = 20.0            # Maximum ratio between boats, land and building pixels

# Filtering Statistics Variables
rejections_condition_1 = 0
rejections_condition_2 = 0
rejections_condition_3 = 0
rejections_condition_4 = 0
accepted = 0

# Miscellaneous
datetime_str = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
results_root = "results_{}".format(datetime_str)

# Argument Parser
parser = argparse.ArgumentParser(description="Process to filter maritime images")
parser.add_argument("IMAGE_FILE_LIST", help="File containing the list of image paths")
parser.add_argument('-p', action="store_true", dest='parallelize', default=True, help="Use process parallelization")
parser.add_argument('-v', action="store_false", dest='verbose', default=False, help="Output log messages for every image")
parser.add_argument('-save_images', action="store_false", dest='save_images', default=False, help="Save image output to file")
parser.add_argument('-output', action="store", dest="output_folder", default="results_{}".format(datetime_str), help="The destination folder for results")

#
# Bounding Box clamping procedure
#
def clamp_bbox(xmin, ymin, xmax, ymax, img_shape):
    """
    Clamps a bounding box to the specified image shape (dimensions)
    @param xmin The minimum x-coordinate value
    @param ymin The minimum y-coordinate value
    @param xmax The maximum x-coordinate value
    @param ymax The maximum y-coordinate value
    @param img_shape The dimensions of the image
    """
    if xmin < 0:
        xmin = 0

    if ymin < 0:
        ymin = 0

    if xmax > (img_shape[1] - 1):
        xmax = img_shape[1] - 1

    if ymax > (img_shape[0] - 1):
        ymax = img_shape[0] - 1

    return xmin, ymin, xmax, ymax


#
# Intersection over Union procedure
#
def bb_intersection_over_union(box_a, box_b):
    """
    Calculates the intersection over union of two bounding boxes
    
    @param box_a The first box 
    @param box_b The second box
    @return The IOU of the boxes
    """

    # Determine the (x, y)-coordinates of the intersection rectangle
    x_a = max(box_a[0], box_b[0])
    y_a = max(box_a[1], box_b[1])
    x_b = min(box_a[2], box_b[2])
    y_b = min(box_a[3], box_b[3])

    # Compute the area of intersection rectangle
    inter_area = max(0, x_b - x_a + 1) * max(0, y_b - y_a + 1)

    # Compute the area of both the prediction and ground-truth
    # rectangles
    box_a_area = (box_a[2] - box_a[0] + 1) * (box_a[3] - box_a[1] + 1)
    box_b_area = (box_b[2] - box_b[0] + 1) * (box_b[3] - box_b[1] + 1)

    # Compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = inter_area / float(box_a_area + box_b_area - inter_area)

    # Return the intersection over union value
    return iou


#
# Filtering procedure
#
def filter_for_boats(filename, dlp, rcnn, verbose=False, save_images=False, executor=None):
    """
    Process images and select the largest maritime vessel on water (if it exists)
    
    @param filename The name of the image to process
    @param dlp The Deep Lab Processor object for segmentation
    @param rcnn The Mask-RCNN object for segmentation
    @param verbose Flag to indicate whether we log the output of this procedure
    @param save_images Flag to indicate whether we should save the intermediate result imagery
    @param executor The ThreadPoolExecutor object for parallel operation
    """
    rejections = [0, 0, 0]
    accepts = 0
    img = cv2.imread(filename)
    file_path = os.path.basename(filename)
    output_file_path = "{}/accept/{}.txt".format(results_root, os.path.splitext(file_path)[0])
        
    if img is not None and img.any() and ~os.path.exists(output_file_path):
        orig_img = img.copy()
        
        if save_images:
            rcnn_output_img = np.zeros(orig_img.shape, dtype=orig_img.dtype)
            dl_output_img = np.zeros(orig_img.shape, dtype=orig_img.dtype)

        status = "REJECTED"
        log_msg = ""

        # [ CONDITION 1 ]
        if img.shape[1] >= 360:
            total_pixels = orig_img.shape[0] * orig_img.shape[1]
            img = mx.nd.array(img)
            boat_index = 8
            person_index = 0

            rcnn_dict = {}
            segment_dict = {}

            if executor is None:
                rcnn_dict = rcnn.get_mask(img, (orig_img.shape[1], orig_img.shape[0]))
                segment_dict = dlp.process(img)

            else:
                rcnn_future = executor.submit(rcnn.get_mask, img, (orig_img.shape[1], orig_img.shape[0]))
                dl_future = executor.submit(dlp.process, img)
                rcnn_dict = rcnn_future.result()
                segment_dict = dl_future.result()
            
            if save_images:
                rcnn_output_img = rcnn_dict["output"]
            
            if boat_index in rcnn_dict:
                rcnn_boat_boxes = np.zeros((len(rcnn_dict[boat_index]), 4))
                rcnn_person_boxes = np.zeros((len(rcnn_dict[person_index]), 4)) if person_index in rcnn_dict else None

                for idx, boat_info in enumerate(rcnn_dict[boat_index]):
                    rcnn_boat_boxes[idx] = boat_info[0]

                if rcnn_person_boxes is not None and rcnn_person_boxes.any():
                    for idx, person_info in enumerate(rcnn_dict[person_index]):
                        rcnn_person_boxes[idx] = person_info[0]

                rcnn_boat_mask = rcnn_dict["boat_mask"]
                rcnn_person_mask = rcnn_dict["person_mask"] if rcnn_person_boxes is not None else np.zeros(rcnn_boat_mask.shape)
                num_boat_pixels = np.sum(np.where(rcnn_boat_mask == 1, 1, 0))
                num_person_pixels = np.sum(np.where(rcnn_person_mask == 1, 1, 0))

                # Get boat and person ratios from MTCNN
                boat_ratio = float(num_boat_pixels / total_pixels) * 100.0
                person_ratio = float(num_person_pixels / total_pixels) * 100.0                
                
                # Replace DeepLab boat and person info with Mask R-CNN data
                predictions = segment_dict["predictions"]
                predictions = np.where(np.isin(predictions, dlp.boat_indices), -1, predictions)
                predictions = np.where(np.isin(predictions, dlp.person_idx), -1, predictions)
                predictions = np.where(rcnn_boat_mask == 1, 76, predictions)
                predictions = np.where(rcnn_person_mask == 1, 12, predictions)
                
                mask = get_color_pallete(predictions, 'ade20k')
                boat_pixels = np.where(np.isin(predictions, dlp.boat_indices), 1, 0)
                boat_pixels_sum = np.sum(boat_pixels)        

                # Update pixel ratios
                _, land_ratio, building_ratio, _, water_ratio, sky_ratio = dlp.get_ratios(predictions, total_pixels)

                # Update DeepLab output dictionary so that output mask is appropriate
                segment_dict["predictions"] = predictions
                segment_dict["boat"] = boat_ratio
                segment_dict["land"] = land_ratio
                segment_dict["building"] = building_ratio
                segment_dict["person"] = person_ratio
                segment_dict["water"] = water_ratio
                segment_dict["sky"] = sky_ratio
                segment_dict["boat_pixels"] = boat_pixels
                segment_dict["mask"] = mask

                # Gather DeepLab output
                if save_images:
                    dl_output_img = dlp.get_output_mask(segment_dict)

                # Get ratios
                land_building_ratio = land_ratio + building_ratio                
                boat_sky_ratio = (boat_ratio + sky_ratio)
                non_boat_sky_ratio = 100.0 - boat_sky_ratio
                lb_non_boat_ratio = (land_building_ratio / non_boat_sky_ratio) * 100.0 if non_boat_sky_ratio > 0 else 0.0
                water_non_boat_ratio = (water_ratio / non_boat_sky_ratio) * 100.0 if non_boat_sky_ratio > 0 else 0.0

                # Output annotations for debugging
                condition2a = "Land/Building Non-Boat Ratio {:.1f} >= {:.1f}".format(lb_non_boat_ratio, LB_NON_BOAT_RATIO_THRESH) if lb_non_boat_ratio >= LB_NON_BOAT_RATIO_THRESH else ""
                condition2b = "Land/Building Image Ratio {:.1f} >= {:.1f}".format(land_building_ratio, LB_RATIO_THRESH) if land_building_ratio >= LB_RATIO_THRESH else ""
                condition2c = "Water Non-Boat Ratio {:.1f} < {:.1f}".format(water_non_boat_ratio, WATER_NON_BOAT_RATIO_THRESH) if water_non_boat_ratio < WATER_NON_BOAT_RATIO_THRESH else ""
                condition2d = "Person Ratio {:.1f} >= {:.1f}".format(person_ratio, PERSON_RATIO_THRESH) if person_ratio >= PERSON_RATIO_THRESH else ""
                condition2e = "Image Water Ratio {:.1f} < 5.0".format(water_ratio) if water_ratio < 5.0 else ""

                # [ CONDITION 2 ]
                if (lb_non_boat_ratio < LB_NON_BOAT_RATIO_THRESH or land_building_ratio < LB_RATIO_THRESH or \
                    water_non_boat_ratio >= WATER_NON_BOAT_RATIO_THRESH) and \
                    person_ratio < PERSON_RATIO_THRESH and water_ratio >= 5.0:
                    
                    largest_boat_bbox = []
                    largest_boat_pixel_cnt = 0
                    
                    # Get largest boat instance
                    for idx, rcnn_bbox in enumerate(rcnn_boat_boxes):
                        xmin, ymin, xmax, ymax = [x for x in rcnn_bbox]
                        xmin, ymin, xmax, ymax = clamp_bbox(xmin, ymin, xmax, ymax, orig_img.shape)
                        region = rcnn_boat_mask[int(ymin): int(ymax + 1), int(xmin): int(xmax + 1)]
                        region_boat_pixel_cnt = np.sum(region)
                        
                        if region_boat_pixel_cnt > largest_boat_pixel_cnt:
                            largest_boat_pixel_cnt = region_boat_pixel_cnt
                            largest_boat_bbox = rcnn_bbox
                            
                    # Get the number of pixels attributed to the largest boat instance
                    xmin, ymin, xmax, ymax = [x for x in largest_boat_bbox]
                    xmin, ymin, xmax, ymax = clamp_bbox(xmin, ymin, xmax, ymax, orig_img.shape)
                    box_w = xmax - xmin
                    box_h = ymax - ymin
                    largest_boat_pixel_region = rcnn_boat_mask[int(ymin): int(ymax + 1), int(xmin): int(xmax + 1)]
                    largest_boat_pixel_cnt = np.sum(largest_boat_pixel_region)
                    largest_boat_pixel_ratio = float(largest_boat_pixel_cnt / total_pixels) * 100.0
                    largest_boat_relative_ratio = float(largest_boat_pixel_cnt / num_boat_pixels) * 100.0 if num_boat_pixels > 0 else 0.0

                    # Get the percentage of land and building pixels in the largest boat region
                    dl_largest_boat_pixel_region = predictions[int(ymin): int(ymax + 1), int(xmin): int(xmax + 1)]
                    br, lr, bldr, pr, wr, sr = dlp.get_ratios(dl_largest_boat_pixel_region, (box_w * box_h))
                    largest_boat_lb_ratio = lr + bldr
                    
                    condition3a = "Largest Boat Ratio {:.1f} < {:.1f}".format(largest_boat_pixel_ratio, BOAT_THRESH) if largest_boat_pixel_ratio < BOAT_THRESH else ""
                    condition3b = "Largest Boat Pixel Count {} < {}".format(int(largest_boat_pixel_cnt), int(BOAT_PIXEL_CNT_THRESH)) if largest_boat_pixel_cnt < BOAT_PIXEL_CNT_THRESH else ""
                    condition3c = "Image Boat Ratio {:.1f} >= {:.1f}".format(boat_ratio, BOAT_UPPER_BOUND_THRESH) if boat_ratio >= BOAT_UPPER_BOUND_THRESH else ""
                    condition3d = "Largest Boat Land/Building Ratio {:.1f} >= {:.1f}".format(largest_boat_lb_ratio, BOAT_LB_THRESHOLD)  if largest_boat_lb_ratio >= BOAT_LB_THRESHOLD  else ""
                    condition3e = "Largest Boat Relative Ratio {:.1f} <= 80.0".format(largest_boat_relative_ratio) if largest_boat_relative_ratio <= 80.0 else ""

                    # [ CONDITION 3 ]
                    if (largest_boat_pixel_ratio >= BOAT_THRESH or largest_boat_pixel_cnt >= BOAT_PIXEL_CNT_THRESH) and \
                       (boat_ratio < BOAT_UPPER_BOUND_THRESH or largest_boat_relative_ratio > 80.0) and largest_boat_lb_ratio < BOAT_LB_THRESHOLD:

                        accepts += 1
                        status = "ACCEPTED"
                        log_msg = "ACCEPTED"

                        output_path = "{}/accept/{}.txt".format(results_root, os.path.splitext(file_path)[0])
                        output_file = open(output_path, "w")
                        output_file.write(f"{filename}, {xmin}, {ymin}, {box_w}, {box_h}\n")
                        output_file.close()

                        if save_images:
                            orig_img = cv2.putText(orig_img, status, (50, 35), cv2.FONT_HERSHEY_SIMPLEX, 0.60,  (0, 255, 0), 2, cv2.LINE_AA)
                        
                    else:
                        rejections[2] += 1
                        status = "REJECTED (3)\n"
                        log_msg = "REJECTED (3): "
                    
                        if len(condition3a) > 0:
                            status += condition3a + "\n"
                            log_msg += condition3a + ","

                        if len(condition3b) > 0:
                            status += condition3b + "\n"
                            log_msg += condition3b + ","

                        if len(condition3c) > 0:
                            status += condition3c + "\n"
                            log_msg += condition3c + ","

                        if len(condition3d) > 0:
                            status += condition3d + "\n"
                            log_msg += condition3d + ","

                        if len(condition3e) > 0:
                            status += condition3e
                            log_msg += condition3e
                        
                        if save_images:
                            y0, dy = 35, 30
                            for i, line in enumerate(status.split('\n')):
                                y = y0 + (i * dy)
                                orig_img = cv2.putText(orig_img, line, (20, y), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 2, cv2.LINE_AA)                                          
                else:
                    # Too much land, building, or people
                    rejections[1] += 1
                    status = "REJECTED (2)\n"
                    log_msg = "REJECTED (2): "

                    if len(condition2a) > 0:
                        status += condition2a + "\n"
                        log_msg += condition2a + ","
                    
                    if len(condition2b) > 0:
                        status += condition2b + "\n"
                        log_msg += condition2b + ","

                    if len(condition2c) > 0:
                        status += condition2c + "\n"
                        log_msg += condition2c + ","

                    if len(condition2d) > 0:
                        status += condition2d + "\n"
                        log_msg += condition2d + ","

                    if len(condition2e) > 0:
                        status += condition2e
                        log_msg += condition2e

                    if save_images:
                        y0, dy = 35, 30
                        for i, line in enumerate(status.split('\n')):
                            y = y0 + (i * dy)
                            orig_img = cv2.putText(orig_img, line, (20, y), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 2, cv2.LINE_AA)                    
                    
            else:
                rejections[1] += 1
                status = "REJECTED (2)\nNo Mask R-CNN Boats"
                log_msg = "REJECTED (2): No Mask R-CNN Boats"

                if save_images:
                    y0, dy = 35, 30
                    for i, line in enumerate(status.split('\n')):
                        y = y0 + (i * dy)
                        orig_img = cv2.putText(orig_img, line, (20, y), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 2, cv2.LINE_AA)  
                
        else:
            # Small image
            rejections[0] += 1
            status = "REJECTED (1)\nWidth < 360"
            log_msg = "REJECTED (1): Width < 360"

            if save_images:
                y0, dy = 35, 30
                for i, line in enumerate(status.split('\n')):
                    y = y0 + (i * dy)
                    orig_img = cv2.putText(orig_img, line, (20, y), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 2, cv2.LINE_AA)

        # Write output image
        if save_images:
            output_img = cv2.hconcat([orig_img, rcnn_output_img, dl_output_img])
            dest_folder = results_root + "/accept" if accept == True else results_root + "/reject"
            output_path = "{}/{}.jpg".format(dest_folder, os.path.splitext(file_path)[0])
            cv2.imwrite(output_path, output_img)

        if verbose:
            logging.info("%s: %s", file_path, log_msg)

        return 0, accepts, rejections
    else:
        return 1, accepts, rejections



# 
# Execute the filtering process in parallel
#
def execute_filtering(file_list, gpu_idx, return_dict, verbose=False, save_images=False):
    """
    Executes the data filtering procedure in parallel using a ThreadPool
    
    @param file_list The file containing the list of image paths (must have one image to a line)
    @param gpu_idx The index of the GPU to use for processing
    @param return_dict A dictionary storing the results of the filtering procedure
    @param verbose Flag to indicate whether logging should be performed
    @param save_images Flag to indicate whether intermediate image results should be saved    
    """
    
    # DeepLabProcessor
    dlp = DeepLabProcessor(use_gpu=True, gpu_idx=gpu_idx)
    
    # Create MaskRCNN object
    rcnn = MaskRCNN(use_gpu=True, gpu_idx=gpu_idx)

    # Stats
    valid = 0;
    invalid = 0;
    files_processed = 0
    img_accepts = 0
    img_rejections = [0, 0, 0]

    # Create thread pool
    with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:        
        for fn in file_list:
            ret, accepts, rejections = filter_for_boats(fn, dlp, rcnn, verbose, save_images, executor)
            img_accepts += accepts
            img_rejections[0] += rejections[0]
            img_rejections[1] += rejections[1]
            img_rejections[2] += rejections[2]
            gc.collect()

            files_processed += 1

            if files_processed % 10000 == 0:
                logging.info("Processed %d images", files_processed)

            if ret == 0:
                valid += 1
            else:
                invalid += 1
        
    return_dict["valid"] += valid
    return_dict["invalid"] += invalid
    return_dict["accepts"] += img_accepts
    return_dict["condition_1_rejections"] += img_rejections[0]
    return_dict["condition_2_rejections"] += img_rejections[1]
    return_dict["condition_3_rejections"] += img_rejections[2]


#------#
# MAIN #
#------#
if __name__ == '__main__':
    gc.enable()
    args = parser.parse_args()
    
    valid = 0
    invalid = 0
    parallelize = False

    # Mark start time
    tic = time.perf_counter()

    # Get output folder
    results_root = args.output_folder

    if not os.path.isdir(results_root):
        os.mkdir(results_root)
        os.mkdir(results_root + "/accept")

        if args.save_images:
            os.mkdir(results_root + "/reject")

    # Set up log
    log_filename = "{}/filter_boat_data.log".format(results_root)
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', filename=log_filename, level=logging.INFO)
    logging.info("Initiating filtering procedure...")
    
    with open(args.IMAGE_FILE_LIST) as fp:
        print()

        NUM_PROCESSES = 4
        manager = multiprocessing.Manager()
        return_dict = manager.dict()

        file_list = []

        # Read list of images from file and store in a list
        for cnt, fn in enumerate(fp.read().splitlines()):
            file_list.append(fn)                

        if args.parallelize:
            processors = []
            list_len = len(file_list)
            workload = list_len // 4
            start_idx = 0
            end_idx = workload + 1

            return_dict["valid"] = 0
            return_dict["invalid"] = 0
            return_dict["accepts"] = 0
            return_dict["condition_1_rejections"] = 0
            return_dict["condition_2_rejections"] = 0
            return_dict["condition_3_rejections"] = 0

            for i in range(NUM_PROCESSES):
                p = multiprocessing.Process(target=execute_filtering, args=(file_list[start_idx : min(end_idx, list_len)], i % 2, return_dict, args.verbose, args.save_images))
                processors.append(p)
                start_idx = end_idx
                end_idx += workload + 1

            for p in processors:
                p.start()

            for p in processors:
                p.join()        

            print("Results:", return_dict)
                    
        else:
            for cnt, fn in enumerate(tqdm(fp.read().splitlines())):
                ret = filter_for_boats(fn)     
                
                if ret == 0:
                    valid += 1
                else:
                    invalid += 1
                
        # Mark end time
        toc = time.perf_counter()

        logging.info('Statistics:')
        logging.info('\tTotal Images: %d', return_dict["valid"])
        logging.info("\tAccepts: %d", return_dict["accepts"])
        logging.info("\tCondition 1 Rejections: %d", return_dict["condition_1_rejections"])
        logging.info("\tCondition 2 Rejections: %d", return_dict["condition_2_rejections"])
        logging.info("\tCondition 3 Rejections: %d", return_dict["condition_3_rejections"])
        logging.info("TOTAL PROCESSING TIME: %0.4f", toc - tic)
        print("[!] Processed {} images. Valid: {}. Invalid: {}. TIME: {}".format(cnt + 1, return_dict["valid"], return_dict["invalid"], toc - tic))
        
        return_dict["valid"]
