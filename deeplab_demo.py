#!/usr/bin/python3
"""3. Test with DeepLabV3 Pre-trained Models
======================================

This is a quick demo of using GluonCV DeepLabV3 model on ADE20K dataset.
Please follow the `installation guide <../../index.html#installation>`__
to install MXNet and GluonCV if not yet.
"""
import sys, os
import numpy as np
import mxnet as mx
from mxnet import image
from mxnet.gluon.data.vision import transforms
from matplotlib import pyplot as plt
import gluoncv
from gluoncv.data.transforms.presets.segmentation import test_transform
from gluoncv.utils.viz import get_color_pallete
import cv2

# using cpu
ctx = mx.cpu(0)

def segment(filename):
    # load the image
    orig_img = cv2.imread(filename)
    img = mx.nd.array(orig_img)  

    # normalize the image using dataset mean    
    img = test_transform(img, ctx)

    # Load the pre-trained model and make prediction
    model = gluoncv.model_zoo.get_model('deeplab_resnet101_ade', pretrained=True)

    # make prediction using single scale
    output = model.predict(img)
    predict = mx.nd.squeeze(mx.nd.argmax(output, 1)).asnumpy()
    
    boat_pixels = np.sum(np.where(predict == 76, 1, 0))
    total_pixels = orig_img.shape[0] * orig_img.shape[1]
    boat_ratio = boat_pixels / total_pixels
    
    print("[*] Number of boat pixels:", boat_pixels)
    print("[*] Boat proportion in image {}: {}".format(filename, boat_ratio))    
    
    # Add color pallete for visualization    
    mask = get_color_pallete(predict, 'ade20k')
    file_path = os.path.basename(filename)
    output_path = "results/{}_deeplab.png".format(os.path.splitext(file_path)[0])
    mask.save(output_path)
 
if __name__ == '__main__':
    if len(sys.argv) == 2:
        segment(sys.argv[1])
    else:
        print(">> Error: Invalid number of arguments provided")

