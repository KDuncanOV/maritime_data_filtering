import numpy as np
import mxnet as mx
import cv2
import time
import threading
from PIL import Image
from mxnet.gluon.data.vision import transforms
import gluoncv
from gluoncv.data.transforms.presets.segmentation import test_transform
from gluoncv.utils.viz import get_color_pallete
from gluoncv import model_zoo, data, utils
from gluoncv.data.transforms.mask import fill


#============================================================================================#
# YOLO:
# You Only Look Once Object Detector
#============================================================================================##
class YOLODetector(object):
    '''
    Performs YOLO object detection
    '''
    def __init__(self, use_gpu=True):
        if use_gpu:
            self.ctx = [mx.gpu(1), mx.gpu(0)]
            print("[!] Initializing YOLO Detector (GPU)...", end=" ")
        else:
            cpu_count = 4  # multiprocessing.cpu_count()
            self.ctx = [mx.cpu(i) for i in range(cpu_count)]
            print("[!] Initializing YOLO Detector (CPU)...", end=" ")

        self.net = gluoncv.model_zoo.get_model('yolo3_darknet53_voc', pretrained=True, ctx=self.ctx)
        self.thread_lock = threading.Lock()
        print("DONE!")

    #
    # Bounding Box clamping procedure
    #
    def clamp_bbox(self, xmin, ymin, xmax, ymax, img_shape):
        if xmin < 0:
            xmin = 0

        if ymin < 0:
            ymin = 0

        if xmax > (img_shape[0] - 1):
            xmax = img_shape[0] - 1

        if ymax > (img_shape[1] - 1):
            ymax = img_shape[1] - 1

        return xmin, ymin, xmax, ymax

    def process(self, img):
        orig_img = img
        orig_w = orig_img.shape[1]
        orig_h = orig_img.shape[0]
        img, _ = gluoncv.data.transforms.presets.yolo.transform_test(img.copy())
        img = img.as_in_context(self.ctx[0])

        test_w = img.shape[3]
        test_h = img.shape[2]

        self.thread_lock.acquire()
        class_IDs, scores, bounding_boxes = self.net(img)
        self.thread_lock.release()

        # We are only processing one image at a time, hence the 0th index
        class_IDs = class_IDs[0].asnumpy()
        scores = scores[0].asnumpy()
        bounding_boxes = bounding_boxes[0].asnumpy()
        filtered_ids = []
        filtered_scores = []
        filtered_bboxes = []

        for i, bbox in enumerate(bounding_boxes):
            if scores is not None and scores.flat[i] < 0.5:
                continue

            filtered_ids += [class_IDs.flat[i]]
            filtered_scores += [scores.flat[i]]

            xmin, ymin, xmax, ymax = [x for x in bbox]
            xmin /= test_w
            xmax /= test_w
            ymin /= test_h
            ymax /= test_h
            xmin *= orig_w
            xmax *= orig_w
            ymin *= orig_h
            ymax *= orig_h
            #xmin, ymin, xmax, ymax = clamp_bbox(xmin, ymin, xmax, ymax, (orig_w, orig_h))
            bbox = np.array([xmin, ymin, xmax, ymax], dtype=bbox.dtype)
            #print("BBox:", bbox)
            filtered_bboxes += [bbox]

        filtered_ids = np.expand_dims(np.asarray(filtered_ids), axis=0)
        filtered_scores = np.expand_dims(np.asarray(filtered_scores), axis=0)
        filtered_bboxes = np.expand_dims(np.asarray(filtered_bboxes), axis=0)

        return filtered_ids, filtered_scores, filtered_bboxes

    def plot_bounding_box(self, img, bboxes, scores, class_ids):
        return gluoncv.utils.viz.cv_plot_bbox(img, bboxes, scores, class_ids, class_names=self.net.classes)
