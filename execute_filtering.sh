#!/bin/bash

# Total number of valid images processed
total_images_processed=0

python3 filter_boat_data.py sub_list_02.txt -output results_20200624-110742

# Loop through lists of images
for i in {1..20}
do
    list_name=$(printf 'sub_list_%02d.txt' "${i}")
    python3 filter_boat_data.py ${list_name}
    num_images_in_file=`cat ${list_name} | wc -l`
    let "total_images_processed+=${num_images_in_file}"
    dt=$(date '+%m/%d/%Y %H:%M:%S');
    echo
    echo "===================================================================="
    echo "[${dt}] (${i}) Total number of images processed: ${total_images_processed} "
    echo "===================================================================="
done

echo
echo "[!] Processing Complete"
