#!/usr/bin/python3

import sys
import os
from os import listdir
import numpy as np
import cv2
import time
import logging
import datetime
import gc
import argparse
from tqdm import tqdm


# Miscellaneous
results_root = "final_results"

# Argument Parser
parser = argparse.ArgumentParser(description="Tool to parse the results of maritime image filtering")
parser.add_argument("RESULTS_FOLDER", help="Folder containing the lists of result folders containing the list of text files.")
parser.add_argument('-d', action="store", dest="output_folder", default="/maritime/kduncan/data_cleaning_results/", help="The destination folder for image results")

#
# Main Entry Point
#
def main(results_folder, output_folder):
    total_files = 0

    if os.path.exists(results_folder) and os.path.isdir(results_folder):
        # Get all results folders
        all_folders = [f for f in os.listdir(results_folder) if os.path.isdir(os.path.join(results_folder, f)) and f.lower().startswith('results')]
        all_folders = [os.path.join(results_folder, f) for f in all_folders]
        print(f"[*] Found {len(all_folders)} result folders")

        # For each result folder read the list of text file results
        for folder in tqdm(all_folders):
            accept_folder = os.path.join(folder, "accept")
            
            if os.path.exists(accept_folder):
                all_text_files = [f for f in os.listdir(accept_folder) if os.path.isfile(os.path.join(accept_folder, f)) and f.lower().endswith('.txt')]
                num_files = len(all_text_files)
                total_files += num_files

                #print(f"Reading {accept_folder} containing {len(all_text_files)}")
                image_name = ""
                bbox = [0, 0, 0, 0]

                for t in all_text_files:
                    orig_file_name = os.path.join(accept_folder, t)
                    dest_sub_folder = t[0:2]
                    dest = os.path.join(output_folder, dest_sub_folder)

                    with open(orig_file_name) as fp:
                        line = fp.readline()
                        values = line.split(",")
                        values = [v.strip() for v in values]
                        image_name = values[0]
                        xmin = float(values[1])
                        ymin = float(values[2])
                        box_w = float(values[3])
                        box_h = float(values[4])
                        xmax = xmin + box_w
                        ymax = ymin + box_h
                        bbox[0] = xmin
                        bbox[1] = ymin
                        bbox[2] = box_w
                        bbox[3] = box_h

                        image_name = image_name.replace("/home/kduncan/Code/ImageRefinement/boats", "/maritime/kduncan/datasets/lsun_boats")

                    new_file_name = os.path.join(dest, t)
                    #print(f"New image location: {image_name}. Sub folder: {dest}")
                    #print(f"New file location:  {new_file_name}")

                    if not os.path.isdir(dest):
                        os.mkdir(dest)

                    output_file = open(new_file_name, "w")
                    output_file.write(f"{image_name}, {bbox[0]}, {bbox[1]}, {bbox[2]}, {bbox[3]}\n")
                    output_file.close()
                    
                    #sys.exit()
                        
                        
        print(f"Total files: {total_files}")

        return 0

    else:
        print(f"[!] {results_folder} is not a valid folder")
        return -1


#-------------#
# Entry Point #
#-------------#
if __name__ == '__main__':
    gc.enable()
    args = parser.parse_args()

    main(args.RESULTS_FOLDER, args.output_folder)
    
    